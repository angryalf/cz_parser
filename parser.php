<?php
//error_reporting(E_ERROR);
require_once 'vendor/autoload.php';

use PHPHtmlParser\Dom;
use Cocur\Slugify\Slugify;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Michelf\Markdown, Michelf\SmartyPants;
use League\HTMLToMarkdown\HtmlConverter;
use League\Url\Url;


$allowed_tags = [
//    "a",
//    "abbr",
//    "acronym",
//    "address",
//    "applet",
//    "area",
//    "article",
//    "aside",
//    "audio",
    "b",
//    "base",
//    "basefont",
//    "bdi",
//    "bdo",
//    "bgsound",
//    "big",
//    "blink",
//    "blockquote",
//    "body",
    "br",
//    "button",
//    "canvas",
//    "caption",
//    "center",
//    "cite",
//    "code",
//    "col",
//    "colgroup",
//    "content",
//    "data",
//    "datalist",
//    "dd",
//    "decorator",
//    "del",
//    "details",
//    "dfn",
//    "dir",
//    "div",
//    "dl",
//    "dt",
//    "element",
    "em",
    "embed",
//    "fieldset",
//    "figcaption",
//    "figure",
//    "font",
//    "footer",
//    "form",
//    "frame",
//    "frameset",
    "h1",
    "h2",
    "h3",
    "h4",
    "h5",
    "h6",
//    "head",
//    "header",
//    "hgroup",
    "hr",
//    "html",
    "i",
//    "iframe",
//    "img",
//    "input",
//    "ins",
//    "isindex",
//    "kbd",
//    "keygen",
//    "label",
//    "legend",
    "li",
//    "link",
    "listing",
//    "main",
//    "map",
//    "mark",
//    "marquee",
//    "menu",
//    "menuitem",
//    "meta",
//    "meter",
//    "nav",
//    "nobr",
//    "noframes",
//    "noscript",
//    "object",
    "ol",
//    "optgroup",
//    "option",
//    "output",
    "p",
//    "param",
//    "plaintext",
    "pre",
//    "progress",
//    "q",
//    "rp",
//    "rt",
//    "ruby",
//    "s",
//    "samp",
//    "script",
//    "section",
//    "select",
//    "shadow",
    "small",
//    "source",
//    "spacer",
    "span",
//    "strike",
    "strong",
//    "style",
//    "sub",
//    "summary",
//    "sup",
//    "table",
//    "tbody",
//    "td",
//    "template",
//    "textarea",
//    "tfoot",
//    "th",
//    "thead",
//    "time",
    "title",
//    "tr",
//    "track",
//    "tt",
    "u",
    "ul",
//    "var",
//    "video",
//    "wbr",
//    "xmp"
];


echo "\nStart script\n";


if (!file_exists("cz-pages.json")) {
    die("Please create right cz-pages.json file.\n");
}

if (file_exists("cz-revs.json")) {
    $revs = json_decode(file_get_contents("revs.json"), true);
} else {
    $revs = [];
}

$pages = json_decode(file_get_contents("cz-pages.json"), true);


foreach ($pages['cz-pages'] as $k => $pageUrl) {

    $dom = new Dom;

    $options = new \PHPHtmlParser\Options();
    $options->setRemoveScripts(true);

    $dom->setOptions($options);

    $dom->loadFromUrl($pageUrl);

    // get title
    $title = $dom->find('.page-header__title--small')[0]->innerHtml();
    echo "\n{$title}\n";

    $logo = $dom->find('.page-header__img')[0]->getAttribute('data-src');
    $url = (string)Url::createFromUrl($pageUrl)->setPath($logo);

    // generate unique slug for
    $slug = makeSlug($title);

    $logoPath = copyFile($url, null, $slug);

    // clenup file
    removeFile("reviews" . DIRECTORY_SEPARATOR . "{$slug}.md");

    // add before all content text document.querySelector(".text.about__text")
    $text = $dom->find('.text.about__text');
    if ($text->count() ){

        $context = html2md(strip_tags($text[0]->innerHtml(), $allowed_tags));
        storeFile("reviews" . DIRECTORY_SEPARATOR . "{$slug}.md", $context, true);
    }

    $text = $dom->find('div.text.text__fragment.js-text-target');

//    echo "\n{$slug}";

    foreach ($text as $item) {
        //make clean txt format and clenup depend on allowed tags (see below)
        $context = html2md(strip_tags($item->innerHtml(), $allowed_tags));
        $context .= "\n\n\n\n\n";

        storeFile("reviews" . DIRECTORY_SEPARATOR . "{$slug}.md", $context, true);
    }


    // check if structure correct for pages
    if (!isset($revs['pages'])) {
        $revs['pages'] = [];
    }

    $revPage = [
        'casino_name' => $title,
        'rev_url' => '',
        'search_key' => '',
        'file_name' => "{$slug}.md",
        'logo' => $logoPath,
        'output_file' => ''
    ];

    $revs['pages'][] = $revPage;
}

storeFile('revs.json', json_encode($revs, JSON_UNESCAPED_UNICODE));

// make minisite pages
generatePage();


echo "\n\nEND script\n";


function generatePage(){
    echo "\nRun Generator\n";
    $result = exec("php generate.php");
}

function makeSlug($name = null, $index = null)
{
    if ($name === null) return null;

    if (strpos($name, "(") === false){
        // name already parsed but exists
    } else {
        preg_match("/\(([\w\s].+)\)/i",$name, $res);
        $name = $res[1];
    }

    $slugify = new Slugify();
    $slug = $slugify->slugify($name, '_');

    $fileName = ($index) ? "{$slug}_{$index}.md" : "{$slug}.md";

    $filesystem = new Filesystem();

    if ($filesystem->exists("reviews" . DIRECTORY_SEPARATOR . "{$fileName}")) {
        $index = ($index == null) ? 1 : $index++;
        return makeSlug($name, 1);
    } else {
        return $slug;
    }
}

function copyFile($fileName = null, $folder = null, $name = null)
{
    $path_parts = pathinfo($fileName);
    $ext = $path_parts['extension'];

    if ($fileName == null) return;
    $filesystem = new Filesystem();

    if (!$name) {
        $name = basename($fileName);
    } else {
        $name .= ".{$ext}";
    }
    if (!$folder) $folder = 'img';

    $newFile = "output" . DIRECTORY_SEPARATOR . "{$folder}" . DIRECTORY_SEPARATOR . $name;


    $filesystem->copy($fileName, $newFile, true);
    return $newFile;
}

function removeFile($name){
    if ($name == null ) return;
    $filesystem = new Filesystem();

    $filesystem->remove($name);
}

function storeFile($fileName = null, $context = null, $append = false)
{
    if ($fileName == null || $context == null) return;
    $filesystem = new Filesystem();

    if ($append) {
        $filesystem->appendToFile($fileName, $context);
    } else {
        $filesystem->dumpFile($fileName, $context);
    }
    return true;
}

function html2md($html = null)
{
    $converter = new HtmlConverter(array('strip_tags' => true));
    $markdown = $converter->convert($html);

    return $markdown;
}

function html2text($html = null)
{
    $options = array(
        'ignore_errors' => true,
        'drop_links' => true,
    );
    return \Soundasleep\Html2Text::convert($html, $options);
}

function md2html($md)
{

    $html = Markdown::defaultTransform($md);
    print_r($html);
}
