<?php
//error_reporting(E_ERROR);
require_once 'vendor/autoload.php';

use PHPHtmlParser\Dom;
use Cocur\Slugify\Slugify;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Michelf\Markdown, Michelf\SmartyPants;
use League\HTMLToMarkdown\HtmlConverter;
use League\Url\Url;

$table_data = [
    [
        'bonus' => "<div class=\"item__bonus-text\">Бонус <span style=\"color: #459657;\">150% до 100 000₽</span></div>",
        'features' => "<div class=\"item__features-text\"><p>Казино №1 в СНГ</p><p>Высокая отдача слотов</p></div>"
    ],
    [
        'bonus' => "<div class=\"item__bonus-text\">Бонус <span style=\"color: #459657;\">150% до 50 000₽</span></div>",
        'features' => "<div class=\"item__features-text\"><p>Казино №2 в СНГ</p><p>Выплата за 10 минут</p></div>"
    ],
    [
        'bonus' => "<div class=\"item__bonus-text\">Бонус 250% до $600 + 125 Фриспинов</div>",
        'features' => "<div class=\"item__features-text\"><p>Вывод за 1 час</p><p>Щедрые бонусы</p><p>Эксклюзивные слоты</p></div>"
    ],
    [
        'bonus' => "<div class=\"item__bonus-text\">Бонус 100% до 30,000﻿ рублей + 50 фриспинов</div>",
        'features' => "<div class=\"item__features-text\"><p>Бонус за регистрацию</p><p>Быстрый вывод денег</p><p>Новинка 2021 года</p><p>Лицензия Кюрасао</p></div>"
    ],
    [
        'bonus' => "<div class=\"item__bonus-text\">Бонус 100% до 50,000﻿ рублей + 100 фриспинов</div>",
        'features' => "<div class=\"item__features-text\"><p>Бездепозитный бонус</p><p>Слоты с хорошей отдачей</p><p>Новинка 2021 года</p><p>Много бонусов на депозит</p><p>Ставки на киберспорт и спорт</p></div>"
    ],
    [
        'bonus' => "<div class=\"item__bonus-text\">Бонус 100%  до 200,000﻿ рублей</div>",
        'features' => "<div class=\"item__features-text\"><p>Вывод без лимитов</p><p>Слоты Novomatic</p><p>Кешбэк 20%</p></div>"
    ],
    [
        'bonus' => "<div class=\"item__bonus-text\">Бонус 100% до $500 + 225 фриспинов</div>",
        'features' => "<div class=\"item__features-text\"><p class=\"sub-title\">Мгновенный вывод</p><p>Бездепозитный бонус</p><p>Авторитетный бренд</p></div>"
    ],
    [
        'bonus' => "<div class=\"item__bonus-text\">Бонус 225% + 225 фриспинов</div>",
        'features' => "<div class=\"item__features-text\"><p>Есть бездепозитный бонус</p><p>Ставки на спорт</p><p>Много бонусов для новичков</p></div>"
    ],
    [
        'bonus' => "<div class=\"item__bonus-text\">Бонус 100% дo 35,000 рублей + 100 фpиcпинoв </div>",
        'features' => "<div class=\"item__features-text\"><p>Бонус за регистрацию</p><p>Регулярные турниры</p><p>Лайв Шоу</p><p>Много Live игр</p></div>"
    ],
    [
        'bonus' => "<div class=\"item__bonus-text\">Бонус 37% до 180,000 рублей + 180 Фриспинов</div>",
        'features' => "<div class=\"item__features-text\"><p>Бонусы с вагером x1</p><p>Бездепозитный бонус</p><p>Слоты Novomatic</p><p>&nbsp;</p></div>"
    ],
    [
        'bonus' => "<div class=\"item__bonus-text\">Бонус 100% до 70,000 рублей</div>",
        'features' => "<div class=\"item__features-text\"><p>Мальтийская лицензия</p><p>Бездепозитный бонус</p><p>Большие лимиты на вывод</p></div>"
    ],
    [
        'bonus' => "<div class=\"item__bonus-text\">Бонус 100% до 100,000 рублей + 200 фриспинов</div>",
        'features' => "<div class=\"item__features-text\"><p>Есть ставки на спорт</p><p>Бонусный пакет из 5 бонусов</p><p>Большие лимиты на вывод</p></div>"
    ],
    [
        'bonus' => "<div class=\"item__bonus-text\">Бонус 100% до 60,000 рублей</div>",
        'features' => "<div class=\"item__features-text\"><p>Новинка</p><p>Бездепозитный бонус</p><p>Турниры</p></div>"
    ],
    [
        'bonus' => "<div class=\"item__bonus-text\">Бонус 100% до 7,000 рублей</div>",
        'features' => "<div class=\"item__features-text\"><p>Быстрый вывод</p><p>Большие лимиты на вывод</p><p>Отличная репутация</p></div>"
    ],
    [
        'bonus' => "<div class=\"item__bonus-text\">Бонус 100% до 7,000 рублей + 20 фриспинов</div>",
        'features' => "<div class=\"item__features-text\"><p>Лимиты на вывод до $100,000</p><p>Ставки на спорт</p><p>Кэшбэк</p></div>"
    ],
    [
        'bonus' => "<div class=\"item__bonus-text\">Бонус 425% до 100,500 рублей + 75 фриспинов</div>",
        'features' => "<div class=\"item__features-text\"><p>Выгодные бонусы</p><p>Регулярные турниры</p><p>Большой выбор слотов</p></div>"
    ],
    [
        'bonus' => "<div class=\"item__bonus-text\">Бонус 50% до 70,000﻿ рублей + 50 фриспинов</div>",
        'features' => "<div class=\"item__features-text\"><p>Бездепозитный бонус</p><p>Много турниров и акций</p><p>Игровые автоматы Igrosoft</p></div>"
    ],
    [
        'bonus' => "<div class=\"item__bonus-text\">Бонус 100% до 100,000 рублей + 200 фриспинов</div>",
        'features' => "<div class=\"item__features-text\"><p>Огромные лимиты на вывод</p><p>Щедрые бонусы для новичков</p><p>Быстрый вывод денег</p><p>Большой выбор слотов</p></div>"
    ],
    [
        'bonus' => "<div class=\"item__bonus-text\">Бонус 100% до 10,000 рублей + 100 Фриспинов</div>",
        'features' => "<div class=\"item__features-text\"><p>Быстрый вывод</p><p>Игра на криптовалюты</p><p>Еженедельные турниры</p></div>"
    ],
    [
        'bonus' => "<div class=\"item__bonus-text\">Бонус 100% до 120,000 рублей + 200 фриспинов</div>",
        'features' => "<div class=\"item__features-text\"><p>Стартовый пакет из 5 бонусов</p><p>Большие лимиты на вывод</p><p>Турниры и лотереи</p></div>"
    ],
    [
        'bonus' => "<div class=\"item__bonus-text\">Бонус 200% до 15,000 рублей</div>",
        'features' => "<div class=\"item__features-text\"><p>300 рублей бесплатно</p><p>Честные выплаты</p><p>Новинка 2019</p><p>Турниры и Лотереи</p></div>"
    ],
    [
        'bonus' => "<div class=\"item__bonus-text\">Бонус 150% до 40,000 рублей + 70 Фриспинов</div>",
        'features' => "<div class=\"item__features-text\"><p>Бездепозитный бонус</p><p>Слоты Novomatic</p><p>Розыгрыши</p></div>"
    ],
    [
        'bonus' => "<div class=\"item__bonus-text\">Бонус 180% до 100,000 рублей</div>",
        'features' => "<div class=\"item__features-text\"><p>Новинка</p><p>Бездепозитный бонус</p><p>Кешбэк 15%</p></div>"
    ],
    [
        'bonus' => "<div class=\"item__bonus-text\">Бонус 125% до 20,000 рублей + 50 Фриспинов</div>",
        'features' => "<div class=\"item__features-text\"><p>Игра на криптовалюты</p><p>3 первых бонуса</p><p>Быстрый вывод</p></div>"
    ],
    [
        'bonus' => "<div class=\"item__bonus-text\">Бонус 100% до 24,000 рублей</div>",
        'features' => "<div class=\"item__features-text\"><p>Скачиваемая версия</p><p>Бездепозитный бонус</p><p>Работает с 2014</p></div>"
    ],
    [
        'bonus' => "<div class=\"item__bonus-text\">Бонус 100% до 24,000 рублей</div>",
        'features' => "<div class=\"item__features-text\"><p>Можно скачать</p><p>Бездепозитный бонус</p><p>Релоад бонус</p></div>"
    ],
];


echo "\nGenerator start\n";


if (!file_exists("revs.json")) {
    die("Please run parser first.\n");
}
$filesystem = new Filesystem();

$pages = json_decode(file_get_contents("revs.json"), true);

if (!isset($pages['pages'])){
    die("Please run parser first.\n");
}

// copy template files
$filesystem->mirror('templates/files', 'output/files');

$subpages = [];


foreach ($pages['pages'] as $k=>$page) {
    $baseName = basename($page['file_name'], ".md");
    $fileName = (trim($page['output_file']) !== '') ? trim($page['output_file']) : "output" . DIRECTORY_SEPARATOR . "pages" . DIRECTORY_SEPARATOR . "{$baseName}.html";

    if ($filesystem->exists("rewrites" . DIRECTORY_SEPARATOR . "{$baseName}.md")) {
        // rewrite exists
        $context = file_get_contents("rewrites" . DIRECTORY_SEPARATOR . "{$baseName}.md");

        $context = preRender($context, $page);

//        print_r($page);
//        print_r($context);

    } else {
        // get original text
        $context = file_get_contents("reviews" . DIRECTORY_SEPARATOR . "{$baseName}.md");
    }



    $data = [
        'context' => md2html($context),
        'casino_name' => $page['casino_name'],
        'search_key' => $page['search_key'],
        'logo' => "/".str_replace('output/','', $page['logo']),
        'rev_url' => ($page['rev_url']) ? $page['rev_url'] : '',
    ];


    storeFile($fileName, renderSubpage('review.twig',$data));




    $subpages[] = [
        'name' => $page['casino_name'],
        'url' => "pages/". basename($fileName),
        'logo' => "/".str_replace('output/','', $page['logo']),
        'tdata' => getTdata($table_data, $k),
    ];

    echo "\n$fileName\n";

}


//print_r($subpages);

storeFile( "output".DIRECTORY_SEPARATOR."index.html", renderSubpage('index.twig',['items' => $subpages]));


echo "\nGenerator ends\n";

function renderSubpage( $template=null, $data=[] ){

    if ($template == null || $data == null) return;

    $loader = new \Twig\Loader\FilesystemLoader('templates');
    $twig = new \Twig\Environment($loader);

    return $twig->render($template, $data);
}

function preRender($template=null, $data){
    $loader = new \Twig\Loader\ArrayLoader([
        'index' => $template,
    ]);
    $twig = new \Twig\Environment($loader);

    return $twig->render('index', $data);
}

function storeFile($fileName = null, $context = null, $append = false)
{
    if ($fileName == null || $context == null) return;
    $filesystem = new Filesystem();

    if ($append) {
        $filesystem->appendToFile($fileName, $context);
    } else {
        $filesystem->dumpFile($fileName, $context);
    }
    return true;
}

function getTdata($tdata=[], $index=0){
    $num = count($tdata);
    if ($num<=$index){
        $index = $index - $num;
        return getTdata($tdata, $index);
    }
    return $tdata[$index];
}


function md2html($md)
{
    $html = Markdown::defaultTransform($md);
    return ($html);
}
